import boto3
import sys
import os
import psycopg2
from sqlalchemy import (
    Column, Integer, String, DateTime, Text, Boolean, ForeignKey, PickleType,
    Index, Float, LargeBinary)

from sqlalchemy.ext.declarative import declarative_base


def update_bucket_tags(bucket_name, key, value):
    s3c = boto3.client('s3')
    s3c.put_bucket_tagging(Bucket=bucket_name, Tagging={
        'TagSet': [
            {
                'Key': key,
                'Value': value
            }
        ]
    })





class Project(object):


    """
    A project is the base class to all subsequent actions

    :param project_name: The name of the project and the bucket used to

    """

    def __init__(self, project_name):
        self.project_name = project_name

    @property
    def project_owner(self):
        return self._project_owner

    @project_owner.setter
    def project_owner(self, owner):
        self._project_owner = owner

    @project_owner.setter
    def project_owner(self):
        return ''

    def project_location(self):
        return 's3'

    def create(self):
        # Create an S3 client
        s3c = boto3.client('s3')

        # Call S3 to list current buckets
        response = s3c.list_buckets()

        # Get a list of all bucket names from the response
        buckets = [bucket['Name'] for bucket in response['Buckets']]

        for b in buckets:
            if b == self.project_name:
                print 'Project already exists'
                return

        s3c.create_bucket(Bucket=self.project_name, CreateBucketConfiguration={
            'LocationConstraint': 'us-west-2'})
        s3c.put_bucket_tagging(Bucket=self.project_name, Tagging={
            'TagSet': [
                {
                    'Key': 'bucket_type',
                    'Value': 'project'
                },
                {
                    'Key': 'environment',
                    'Value': 'prod'
                }
            ]
        })

    def upload(self, type, source_dir):

        self.create()

        # Create an S3 client
        s3c = boto3.client('s3')

        if type == 'url':
            print 'URL %s' % source_dir

        if type == 'file':
            print 'File %s' % source_dir

            destination = self.project_name

            # enumerate local files recursively
            for root, dirs, files in os.walk(source_dir):

                for filename in files:

                    # construct the full local path
                    local_path = os.path.join(root, filename)

                    # construct the full Dropbox path
                    relative_path = os.path.relpath(local_path, source_dir)
                    s3_path = os.path.join(destination, relative_path)

                    print "Uploading %s..." % s3_path
                    s3c.upload_file(local_path, self.project_name, s3_path)

            # Uploading
            s3c.upload_file(os.path.join(root, file), self.project_name, file)

from abc import ABCMeta, abstractmethod, abstractproperty

class BaseModel(object):
    """
    Base Model object that the Model object inherits from
    """
    __metaclass__ = ABCMeta

    @abstractproperty
    def model_id(self):
        """
        :return: the model_id
        :rtype: unicode
        """
        raise NotImplementedError()

    @abstractproperty
    def model_type(self):
        """
        :return: the model_type
        :rtype: unicode
        """
        raise NotImplementedError()

    @abstractproperty
    def realization_ids(self):
        """
        :return: A list of the realization_id that are in this model
        :rtype: List[unicode]
        """
        raise NotImplementedError()

    @abstractproperty
    def full_filepath(self):
        """
        :return: The absolute path to the file that contains this model
        :rtype: unicode
        """
        raise NotImplementedError()

class BaseModelBag(object):
    """
    A collection of Models with the 
    """
    @abstractproperty
    def model_ids(self):
        """
        :return: the model_id
        :rtype: List[unicode]
        """
        raise NotImplementedError()





class Model(Project):
    """


    """

    def __init__(self):
        self.real_num = ''
        self.model_id = ''





    @property
    def model_type(self):
        return 'model type'

    @property
    def realization_num(self):
        return ''


TERRA_HEADER = """\



      ___      _______                                               
 ____/  /____ /  ____/                                        
/____   ____//  /___                              
    /  /    /  ____/                                 
   /  /    /  /___                                                   
  /__/    /______/                                  
"""





class Run(Model):
    """
    Sub class of model signifying the results of a Model forward simulation
    """

    def __init__(self, simulator):
        self.simulator = simulator



class Realization(Model):
    """

    :param :

    """

    def __init__(self, real_num):
        self.real_num = real_num


    def __iter__(self):
        return self

    def next(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]

f = Project('terra-testing3')



