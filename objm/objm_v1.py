from sqlalchemy import (MetaData, Table, Column, Integer, String, ForeignKey, Date, Boolean,
                        create_engine)

from sqlalchemy.orm import (mapper, relationship, sessionmaker)

from datetime import datetime

import abc

meta = MetaData(schema="test")
schema = 'test'
project_table = Table('projects', meta,
                      Column('id', Integer, primary_key=True),
                      Column('project_name', String(20), nullable=False, key='name'),
                      Column('s3_bucket')
                      )

models_table = Table('models', meta,
                     Column('id', Integer, primary_key=True),
                     Column('name', String(50), nullable=False, key='name'),
                     Column('model_type', String, nullable=False, key='model_type'),
                     Column('last_run_date', Date, key='last_run_date'),
                     schema=schema)

runs_table = Table('runs', meta,
                   Column('id', Integer, primary_key=True),
                   Column('name', String(50), nullable=False, key='name'),
                   Column('running', Boolean, key='running'),
                   Column('real_id', Integer, ForeignKey('reals.id', ondelete="CASCADE")),
                   Column('config', String, key='config'),
                   schema=schema)

sources_table = Table('runs', meta,
                      Column('id', Integer, primary_key=True),
                      Column('name', String(50), nullable=False, key='name'),
                      Column('running', Boolean, key='running'),
                      Column('real_id', Integer, ForeignKey('reals.id', ondelete="CASCADE")),
                      Column('config', String, key='config'),
                      schema=schema)


class Model(object):
    now = datetime.now()

    def __init__(self, name, org_name, model_type):
        self.name = name
        self.org_name = org_name
        self.model_type = model_type

    def last_run_date(self):
        return self.now

    # @abc.abstractmethod
    # def model_type(self):
    #     """Returns abstract class to"""
    #     return self.model_type

    # @staticmethod
    # def import_model(self):
    #     return self
    #
    # @staticmethod
    # def export_model(self):
    #     return self


class Realiz(Model):
    def __init__(self, real_num, params):
        self.real_num = real_num
        self.running = True
        self.params = params

    def model_type(self):
        """Returns abstract class to"""
        return self.model_type

    def __repr__(self):
        return "Realization %s"

    # def type(self):
    #     return self

    # @classmethod
    # def single_realization(cls):
    #     return cls()


class Run(Realiz):
    def __init__(self, name, service, config):
        self.name = name
        self.running = True
        self.service = service
        self.config = config


class Iter(Run):
    def __init__(self, iter_num, data):
        self.iter_num = iter_num
        self.running = True
        self.data = data


mapper(Model, model_table, properties={
    'reals': relationship(Realiz,
                          lazy="dynamic",
                          cascade="all, delete-orphan",
                          passive_deletes=True,
                          )
}
       )

mapper(Realiz, real_table, properties={
    'runs': relationship(Run,
                         lazy="dynamic",
                         cascade="all, delete-orphan",
                         passive_deletes=True,
                         )
}
       )

mapper(Run, run_table, properties={
    'iters': relationship(Iter,
                          lazy="dynamic",
                          cascade="all, delete-orphan",
                          passive_deletes=True,
                          )
}
       )

mapper(Iter, iter_table)

if __name__ == '__main__':
    url = 'postgres+psycopg2://terra:fundus500k@fetl-ds.c4lstuf6msdr.us-west-2.rds.amazonaws.com:5432/metl'
    engine = create_engine(url, echo=True)
    meta.create_all(engine)

    # expire_on_commit=False means the session contents
    # will not get invalidated after commit.
    sess = sessionmaker(engine, expire_on_commit=False)()

    mod = Model('Mod 1', 'org1', 'multi-realization')
    realiz = Realiz(1, 'params')
    run = Run('Run 1', 'service', 'config')
    iter = Iter(1, 'data')

    # mod.last_run_date = datetime.now()
    # mod.realiz.append(Realiz(1, 'params'))
    # mod.runs[0].iters.append(Iter(1,'d'))
    # mod.runs[0].iters[0].reals.append(Realiz(1, 'data'))

    sess.add(mod)
    sess.add(realiz)
    sess.add(run)
    sess.add(iter)

    print("-------------------------\nflush one - save mod + 3 runs\n")
    sess.commit()
    # sess.delete(mod)
    # print("-------------------------\nflush three - delete mod, delete runs in one statement\n")
    # sess.commit()
    sess.close()
