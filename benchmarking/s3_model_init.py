
import boto3
import botocore


def init_model(project, model, bucket = 'terraai-projects'):

    # Create project subfolder
    client = boto3.client('s3')
    try:
        client.put_object(
            Body='',
            Bucket=bucket,
            Key='/'.join([project, model, 'Sources/'])
        )

        client.put_object(
            Body='',
            Bucket=bucket,
            Key='/'.join([project, model, 'Runs/'])
        )
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise


if __name__ == "__main__":
    init_model('Olympus', 'Well_Placement')
