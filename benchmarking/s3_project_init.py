
import boto3


def init_project(project, bucket = 'terraai-projects'):

    # Create project subfolder
    client = boto3.client('s3')
    client.put_object(
        Body='',
        Bucket=bucket,
        Key=project + '/'
    )

if __name__ == "__main__":
    init_project('test')
