import os
import boto3
from boto3.s3.transfer import S3Transfer
import botocore
from boto3 import client

def get_local_file_list(source_dir):

    # Get files in model directory
    if os.path.exists(base_dir):
        files = os.listdir(source_dir)

    file_list = []
    for f in files:
        if f[:1] != '.':
            file_list.append(f)

    print(file_list)
    return file_list


def push_ensemble_to_s3(project, model, model_sub_folder, run_id, source_dir, bucket='terraai-projects'):
    # '/Users/robcannon/Documents/Olympus/Well_Placement/Inputs'
    folder_list = get_local_file_list(source_dir)
    for folder in folder_list:
        print(folder)
        file_list = get_local_file_list('/'.join([source_dir, folder]))
        for file in file_list:
            push_file_to_s3(project, model, model_sub_folder, run_id, source_dir, file, bucket)


def push_folder_to_s3(project, model, model_sub_folder, run_id, source_dir, folder,bucket='terraai-projects'):
    file_list = get_local_file_list('/'.join([source_dir, folder]))
    for file in file_list:
        push_file_to_s3(project, model, model_sub_folder, run_id, source_dir, file, bucket)


def push_file_to_s3(project, model, model_sub_folder, run_id, source_dir, file_name, bucket='terraai-projects'):

    client = boto3.client('s3')

    print('Pushing to s3 '
          '\nsource directory = %s \nfile name = %s \nbucket = %s '
          '\nmodel = %s \n model sub folder = %s \nrun_id = %s'
          % (source_dir, file_name, bucket, model, model_sub_folder, run_id))

    file_path = '/'.join([source_dir, project, model, model_sub_folder, run_id, file_name])
    print(file_path)
    transfer = S3Transfer(client)
    transfer.upload_file(file_path, bucket, '/'.join([project, model, model_sub_folder, run_id, file_name]))


def get_project_list_s3(bucket='terraai-projects'):
    print("Getting project list")
    conn = client('s3')
    for key in conn.list_objects(Bucket=bucket, Key='/'.join([]))['Contents']:
        print(key['Key'])


def get_model_list_s3(project, bucket='terraai-projects'):
    print("Getting model list")
    conn = client('s3')
    for key in conn.list_objects(Bucket=bucket, Key='/'.join([]))['Contents']:
        print(key['Key'])


def get_run_list_s3(bucket, project, model):
    print("Getting run list")
    conn = client('s3')
    for key in conn.list_objects(Bucket=bucket, Key='/'.join([project, model, 'Runs']))['Contents']:
        print(key['Key'])


def get_model_s3():
    print('Getting from s3 \nmodel = %s')


if __name__ == "__main__":
    base_dir = '/Users/robcannon/Documents'
    # model_list = get_model_list_local(base_dir)
    get_local_file_list('/'.join([base_dir, 'Olympus', 'Well_Placement', 'Inputs', 'ECLIPSE']))
    project = 'Olympus'
    model = 'Well_Placement'
    model_sub_folder = 'Inputs'
    run_id = 'Inputs/ECLIPSE'
    source_dir = base_dir
    push_ensemble_to_s3(project, model, model_sub_folder, run_id, source_dir, bucket='terraai-projects')
    # push_model_list_s3(base_dir=base_dir, project='Olympus', model='Well_Placement', sub_directory='Inputs/ECLIPSE', zip=True)