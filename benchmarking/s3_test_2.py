
import boto3
from boto3.s3.transfer import S3Transfer


def init_model(project, model, bucket = 'terraai-projects'):

    # Create project subfolder
    client = boto3.client('s3')
    # with open('/Users/robcannon/Documents/Olympus/Well_Placement/Inputs/ECLIPSE/OLYMPUS/OLYMPUS_GRID.GRDECL') as f:
    #     try:
    #         client.put_object(
    #             Body=f,
    #             Bucket=bucket,
    #             Key='/'.join([project, model, 'Sources/'])
    #         )
    #
    #     except botocore.exceptions.ClientError as e:
    #         if e.response['Error']['Code'] == "404":
    #             print("The object does not exist.")
    #         else:
    #             raise

    file_path = '/Users/robcannon/Documents/Olympus/Well_Placement/Inputs/ECLIPSE/OLYMPUS/OLYMPUS_GRID.GRDECL'
    transfer = S3Transfer(client)
    transfer.upload_file(file_path, bucket, '/'.join([project, model, 'Runs', 'OLYMPUS_GRID.GRDECL']))

if __name__ == "__main__":
    init_model('Olympus', 'Well_Placement')
