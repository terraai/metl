import os
import boto3
import botocore
import shutil

from boto3 import client

def get_model_list_local(source_dir):

    # Get files in model directory
    if os.path.exists(base_dir):
        files = os.listdir(source_dir)

    model_list = []
    for f in files:
        if f[:1] != '.':
            model_list.append(f)

    return model_list


def push_to_s3(file, bucket, prefix):
    s3 = boto3.resource('s3')
    print('Pushing to s3 \nfile = %s \nbucket = %s \nprefix = %s' % (file, bucket, prefix))
    try:
        s3.Bucket(bucket).put_object(Body=file, Key=prefix+'/'+file)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise


def push_model_list_s3(base_dir, project, model, sub_directory, bucket = 'benchmark-models', s3_prefix = None, zip = False):

    # The local directory
    source_dir = '/'.join([base_dir, project, model, sub_directory])
    print(source_dir)

    # get_model_list_local()
    model_list = get_model_list_local(source_dir)

    # The directory to be pushing to s3
    if not s3_prefix:
        s3_prefix = '/'.join([project, model, sub_directory])

    # Iterate though models in list and puch to s3 selectively
    for m in model_list:

        if zip:
            shutil.make_archive(m, 'zip', base_dir)
            push_to_s3(m + '.zip', bucket, s3_prefix)
            os.remove(m + '.zip')
        else:
            model_file_list = get_model_list_local('/'.join([source_dir, m]))
            for file in range(0, len(model_file_list)):
                push_to_s3(file, bucket, '/'.join([s3_prefix, file]))


    print('Pushed %s' % m)


def get_model_list_s3(bucket, prefix):

    conn = client('s3')
    for key in conn.list_objects(Bucket=bucket)['Contents']:
        print(key['Key'])

def get_model_s3(key, bucket = None, prefix = None):

    s3 = boto3.resource('s3')

    try:
        s3.Bucket(bucket).download_file(key, 'example.zip')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise


if __name__ == "__main__":
    base_dir = '/Users/robcannon/Documents'
    # model_list = get_model_list_local(base_dir)
    push_model_list_s3(base_dir=base_dir, project='Olympus', model='Well_Placement', sub_directory='Inputs/ECLIPSE', zip=True)