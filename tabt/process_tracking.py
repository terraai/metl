

import psycopg2
from json import json

# DB Credentials***********
path = "/home/ubuntu/airflow/dags/dag_sched.json"
with open(path, 'r') as conf:
    cred = json.load(conf)


def establish_DBconn():
    conn = psycopg2.connect(database=cred['airflow']['terra_process']['db'],
                            user=cred['airflow']['terra_process']['user'],
                            password=cred['airflow']['terra_process']["password"],
                            host=cred['airflow']['terra_process']['host'],
                            port=config['airflow']['terra_process']['port'])
    cursor = conn.cursor()
    return conn, cursor

conn, cursor = establish_DBconn()

def task_tracker_init_queries(run_id, task_id, realization_num, iteration_num, worker_ip):
    # --Init
    query = """INSERT INTO tabt.task_tracker_dev1(run_id, task_id, realization_num, iteration_num, s_time, ' \
            'f_time, worker_ip) VALUES ('{}', null, null, null, now(), null, null): """.format(run_id)
    print query
    return query



#

# cursor.execute(query)
# conn.commit()
# conn.close()

if __name__ == "__main__":
    task_tracker_init_queries(1, 'null', 'null', 'null', 'null')
