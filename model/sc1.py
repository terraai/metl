# import json
# with open('mac-testing.json') as json_data:
#     cfg = json.load(json_data)
#
# print(cfg['project'])



class Project(object):

    def init_from_json_cfg(self, cfg_path):
        import json

        with open(cfg_path) as json_data:
            cfg = json.load(json_data)

        self.name = cfg['project']['name']
        self.directory = cfg['project']['directory']

    def init_s3(self, bucket='terraai-projects'):
        if not self.name:
            print('Have not initialized the project')
            raise ValueError

        import boto3
        # Create project subfolder
        client = boto3.client('s3')
        client.put_object(
            Body='',
            Bucket=bucket,
            Key=self.name + '/'
        )


class Model(object):

    def init_from_json_cfg(self, cfg_path):
        import json

        with open(cfg_path) as json_data:
            cfg = json.load(json_data)

        self.name = cfg['model']['name']
        self.format = cfg['model']['format']
        self.


    def push_to_s3(project, model, model_sub_folder, run_id, source_dir, file_name, bucket='terraai-projects'):
        import boto3
        from boto3.s3.transfer import S3Transfer
        client = boto3.client('s3')

        print('Pushing to s3 '
              '\nsource directory = %s \nfile name = %s \nbucket = %s '
              '\nmodel = %s \n model sub folder = %s \nrun_id = %s'
              % (source_dir, file_name, bucket, model, model_sub_folder, run_id))

        file_path = '/'.join([source_dir, project, model, model_sub_folder, run_id, file_name])
        print(file_path)
        transfer = S3Transfer(client)
        transfer.upload_file(file_path, bucket, '/'.join([project, model, model_sub_folder, run_id, file_name]))


class Run(object):

    def init_from_json_cfg(self, cfg_path):
        import json

        with open(cfg_path) as json_data:
            cfg = json.load(json_data)


proj = Project()
proj.init_from_json_cfg('mac-testing.json')

print(proj.name)