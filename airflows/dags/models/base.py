

from sqlalchemy import (MetaData, Table, Column, Integer, String, ForeignKey, Date, Boolean, create_engine)
from sqlalchemy.orm import (mapper, relationship, sessionmaker)

from datetime import datetime

meta = MetaData(schema="test")
schema = 'test'

model_table = Table('models', meta,
                    Column('id', Integer, primary_key=True),
                    Column('name', String(50), nullable=False, key='name'),
                    Column('org_name', String, nullable=False, key='org_name'),
                    Column('last_run_date', Date, key='last_run_date'),
                    Column('billing_id', String),
                    schema=schema)

run_table = Table('runs', meta,
                  Column('id', Integer, primary_key=True),
                  Column('name', String(50), nullable=False, key='name'),
                  Column('running', Boolean, key='running'),
                  Column('model_id', Integer, ForeignKey('models.id', ondelete="CASCADE")),
                  Column('service', String, key='service'),
                  Column('config', String, key='config'),
                  schema=schema)

iter_table = Table('iters', meta,
                   Column('id', Integer, primary_key=True),
                   Column('iter_num', Integer, nullable=False, key='iter_num'),
                   Column('running', Boolean, key='running'),
                   Column('run_id', Integer, ForeignKey('run.id', ondelete="CASCADE")),
                   Column('model_id', Integer, ForeignKey('models.id', ondelete="CASCADE")),
                   Column('data', String, key='data'),
                   schema=schema)


real_table = Table('realizs', meta,
                  Column('id', Integer, primary_key=True),
                  Column('real_num', Integer, nullable=False, key='real_num'),
                  Column('running', Boolean, key='running'),
                  Column('iter_id', Integer, ForeignKey('iter.id', ondelete="CASCADE")),
                  Column('run_id', Integer, ForeignKey('run.id', ondelete="CASCADE")),
                  Column('model_id', Integer, ForeignKey('models.id', ondelete="CASCADE")),
                  Column('params', String, key='params'),
                  schema=schema)


class Model(object):
    def __init__(self, name, org_name):
        self.name = name
        self.org_name = org_name
    last_run_date = datetime.now()


class Run(object):
    def __init__(self, name, service, config):
        self.name = name
        self.running = True
        self.service = service
        self.config = config


class Iter(object):
    def __init__(self, iter_num, data):
        self.iter_num = iter_num
        self.running = True
        self.data = data


class Realiz(object):
    def __init__(self, real_num, params):
        self.real_num = real_num
        self.running = True
        self.params = params


mapper(Model, model_table, properties={
    'runs': relationship(Run,
                         lazy="dynamic",
                         cascade="all, delete-orphan",
                         passive_deletes=True,
                         )
}
       )

mapper(Run, run_table, properties={
    'iters': relationship(Iter, lazy="dynamic",
                          cascade="all, delete-orphan",
                          passive_deletes=True,
                          )
}
       )

mapper(Iter, iter_table, properties={
    'realizs': relationship(Iter, lazy="dynamic",
                          cascade="all, delete-orphan",
                          passive_deletes=True,
                          )
}
       )

mapper(Realiz, real_table)

if __name__ == '__main__':
    url = 'postgresql://airflow:NavjotNavjot@airflow-fdo.c4lstuf6msdr.us-west-2.rds.amazonaws.com:5432/airflow_fdo'
    engine = create_engine(url, echo=True)
    meta.create_all(engine)

    # expire_on_commit=False means the session contents
    # will not get invalidated after commit.
    sess = sessionmaker(engine, expire_on_commit=False)()

    # create org with some runs

    # mod = Model()

    mod = Model('Mod 1', 'org1')
    # mod.name = 'Mod 1'
    # mod.org_name = 'org1'
    mod.last_run_date = datetime.now()
    mod.runs.append(Run('WEEE', 'mee', 'pee'))

    sess.add(mod)

    print("-------------------------\nflush one - save mod + 3 runs\n")
    sess.commit()
    # sess.delete(mod)
    # print("-------------------------\nflush three - delete mod, delete runs in one statement\n")
    # sess.commit()
    # sess.close()



# class Iteration(object):
#     def __init__(self, name, current, max_iter):
#         self.name = name
#         self.current = current
#         self.max_iter = max_iter
#
# class Realization(object):
#     def __init__(self, name):
#         self.name = name
