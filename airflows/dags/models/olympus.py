

# Move this OUT
def olympus_api(i, r):
    """
    :param i: iteration
    :param r: realization
    :return: model_dir, model_name
    """
    if i == 1:
        model_dir = 'OLYMPUS_' + str(r) + '/'
        model_name = 'OLYMPUS_' + str(r) + '.DATA'
    else:
        model_dir = 'OLYMPUS_' + str(r) + '_' + str(i) + '/'
        model_name = 'OLYMPUS_' + str(r) + '_' + str(i) + '.DATA'
    return model_dir, model_name


