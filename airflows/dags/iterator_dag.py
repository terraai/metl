# Airflow imports
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

# Python imports
from datetime import datetime, timedelta
import sys
import os
import json

# terra packages
import sim_executors.echelon
from models.olympus import olympus_api

"""
----Params
"""

with open('iterator.json') as f:
    config = json.load(f)

sim_path = '/opt/echelon/bin/echelon '
project_dir = '/usr/local/airflow/afefs/projects/olympus/OLYMPUS/'

# Replace this later with a generalized project path api
model_dir, model_name = olympus_api(1,2)
model_path = project_dir + model_dir + model_name

projects = config['project']
models = config['project']['model']
runs = config['project']['model']['run']
iters = config['project']['model']['run']['iteration']
reals = config['project']['model']['run']['iteration']['realization']

# PROJECTS
project_name = projects['project_name']
# MODELS
model_name = models['model_name']
# RUNS
run_id = runs['run_id']
run_dir = runs['run_dir']
run_output = runs['run_output']
#  ITERATIONS
current_iter = iters['iter_cur']
max_iter = config['project']['model']['run']['iteration']['iter_max']

# REALIZATIONS
current_real = config['project']['model']['run']['iteration']['realization']['real_cur']
max_real = config['project']['model']['run']['iteration']['realization']['real_max']


def make_runs_dir(run_id):
    # First check to see if the run dir has been provided
    if run_id is not None:
        run_path = '/usr/local/airflow/afefs/projects/olympus/runs' + '/' + str(run_id)
        if os.path.exists(run_path):
            for run_id in range(1, 20):
                run_path = '/usr/local/airflow/afefs/projects/olympus/runs' + '_' + str(run_id)
                if not os.path.exists(run_path):
                    try:

                    except:

    if not os.path.exists(run_path):
        os.system('mkdir ' + run_path)

        for run_id in range(1, 20):
            run_path = '/usr/local/airflow/afefs/projects/olympus/runs' + '_' + str(run_id)
            if not os.path.exists(run_path):
            try:

            except:

    else:
        run_path = run_dir + run_id

    if not os.path.exists(run_path):



default_args = {
    'owner': 'airflow',
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    dag_id='olympus_simple_1',
    start_date='@once',
    schedule_interval=timedelta(1),
    default_args=default_args
)


def name_sim_task(i, r, naming_convention=None):
    if (naming_convention is None) or (naming_convention == 'default'):
        return 'iter_' + str(i) + '-real_' + str(r)
    else:
        raise ValueError('Unrecognized naming convention')

sim_tasks = []
agg_tasks = []

for i in range(current_iter, max_iter+1):
    """
    Build list of lists storing task objects to be scheduled in a later loop 
    
    """
    # Aggregation loop
    temp_sim_tasks = []
    # Temporary list of sims
    for r in range(current_real, max_real+1):
        # Realization loop
        temp_sim_tasks.append(PythonOperator(
            task_id='sim-task-'+str(r)+'-agg-task-'+str(i),
            python_callable=test_call(i),
            # params=params,
            queue=queue,
            dag = dag
        ))

    # List of lists stored for later reference
    sim_tasks.append(temp_sim_tasks)
    agg_tasks.append(PythonOperator(
        task_id='agg-task-' + str(i),
        # python_callable=testing_agg(max_real, sim_dir, sim_dir+'agg_'+str(i), sim_tasks[], 'agg_' + name_sim_task(1, r) + '.txt'),
        python_callable= echelon_run(i,r),
        # params='',
        # queue='testing',
        dag=dag
    ))

for i in range(current_iter, max_iter+1):
    """
    Schedule list of lists within each loop 
    """
    for r in range(current_real, max_real+1):
        print str(r) + ' Real +' + str(i) + ' Iter'
        sim_tasks[i-1][r-1].set_downstream(agg_tasks[i-1])
        if i!=max_iter:
            print 'Sim DS ' + str(r) + ' Real +' + str(i) + ' Iter'
            agg_tasks[i-1].set_downstream(sim_tasks[i][r - 1])
            print agg_tasks[i-1].task_id


    # token = 'xoxp-8060700977-8188113159-97403340849-2f9d2b42f63a05101f5e5d8cbdd4a597'
    # Update_Slack = SlackAPIPostOperator(
    #     task_id='notify',
    #     token=token,
    #     channel='git',
    #     text='Finished Test Run -',
    #     dag=dag
    # )


