"""
Code that goes along with the Airflow located at:
http://airflow.readthedocs.org/en/latest/tutorial.html
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta


sim_dir = '/opt/echelon/bin/echelon '
project_dir = '/usr/local/airflow/afefs/projects/olympus/OLYMPUS/'
model_dir = 'OLYMPUS_1/OLYMPUS_1.DATA '

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': '@once',
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    'tutorial_two', default_args=default_args, schedule_interval=timedelta(1))

# t1, t2 and t3 are examples of tasks created by instantiating operators
t1 = BashOperator(
    task_id='show_dir',
    bash_command='cd /usr/local/airflow/afefs/projects/olympus/ && ls -la',
    dag=dag)


t2 = BashOperator(
    task_id='simulation',
    bash_command=sim_dir + model_dir + project_dir,
    params={'my_param': 'Parameter I passed in'},
    dag=dag,
    queue='gpu')

t2.set_upstream(t1)
